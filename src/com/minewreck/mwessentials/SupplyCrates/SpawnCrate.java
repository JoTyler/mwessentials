package com.minewreck.mwessentials.supplycrates;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.minewreck.mwessentials.Main;



public class SpawnCrate implements CommandExecutor,Listener {
		
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {

		
		if(!(sender instanceof Player)){
			GenerateCrate();
			return true;
		}
		
		if(args.length == 1){

			Player player = (Player) sender;
		if(player.isOp()){
			if(args[0].equalsIgnoreCase("spawn")){
		GenerateCrate();
		}}}else{
			Player player = (Player) sender;
			if(Main.crateLoc.size() == 0){
	        	player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&b&lThere are no carepackages currently on the map"));

			}
		for(Location loc : Main.crateLoc){
			int x = loc.getBlockX();
			int y = loc.getBlockY();
			int z = loc.getBlockZ();
        	player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&b&lCarepackage at X:" +x+" Y:"+y+ " Z:"+ z));

		}
		
		}

 
		return false;
		
	}
	int crates = 10;
	Random rand = new Random();
	public void GenerateCrate(){
		
		int PreX= rand.nextInt((9000 - 2000) + 1) + 2000;
		int PreZ= rand.nextInt((9000 - 2000) + 1) + 2000;
		int NegX = rand.nextInt((2-1)+ 1)+ 1;
		int NegZ = rand.nextInt((2-1)+ 1)+ 1;
		int x;
		int z;
		if(NegX == 1){
			x = PreX * (-1);
		}else{
			x=PreX;
		}
		if(NegZ == 1){
			z = PreZ *(-1);
		}else{
			z=PreZ;
		}
		
		int size = Main.crateLoc.size();
		if(crates > size){
			int y;
		    for(y= 256; y>0; y--){
		    	Location loc = new Location(Bukkit.getWorld("world"),x, y ,z);
		    	Block block = loc.getBlock();
		    	if(block.getType().equals(Material.AIR)){
		    		
		    	}else{
		    		int crate = y +1;
		    		y=0;
		    	
		    		Location loc2 = new Location(Bukkit.getWorld("world"),x,crate,z);
		        	Block block2 = loc2.getBlock();
		        	Main.crateLoc.add(block2.getLocation());
		        	block2.setType(Material.BEDROCK);
		        	Bukkit.getServer().broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&b&lCarepackage spawned at X:" +x+" Y:"+crate+ " Z:"+ z));
		    	}	
		    }	
		}
	}

	
	@EventHandler
	public void OpenCrate(PlayerInteractEvent e){
		
		if(e.getAction() == Action.RIGHT_CLICK_BLOCK || e.getAction() == Action.LEFT_CLICK_BLOCK){
			
			if(Main.crateLoc.contains(e.getClickedBlock().getLocation())){
				Main.crateLoc.remove(e.getClickedBlock().getLocation());
				Location loc = e.getClickedBlock().getLocation();
				Bukkit.getServer().broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&b&l" + e.getPlayer().getName() + " has claimed the carepackage at X:" +loc.getBlockX()+" Y:"+loc.getBlockY()+ " Z:"+ loc.getBlockZ()));
				ItemStack Helm = new ItemStack(Material.DIAMOND_HELMET, 1);
				Helm.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 5);
				ItemStack Chest = new ItemStack(Material.DIAMOND_CHESTPLATE, 1);
				Chest.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 5);
				ItemStack Leg = new ItemStack(Material.DIAMOND_LEGGINGS, 1);
				Leg.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 5);
				ItemStack Boot = new ItemStack(Material.DIAMOND_BOOTS, 1);
				Boot.addUnsafeEnchantment(Enchantment.PROTECTION_ENVIRONMENTAL, 5);
				
				e.getPlayer().getWorld().dropItem(e.getClickedBlock().getLocation(), Helm);
				e.getPlayer().getWorld().dropItem(e.getClickedBlock().getLocation(), Chest);
				e.getPlayer().getWorld().dropItem(e.getClickedBlock().getLocation(), Leg);
				e.getPlayer().getWorld().dropItem(e.getClickedBlock().getLocation(), Boot);
				e.getPlayer().getWorld().dropItem(e.getClickedBlock().getLocation(), new ItemStack(Material.DIAMOND_BLOCK, 3));
				e.getPlayer().getWorld().dropItem(e.getClickedBlock().getLocation(), new ItemStack(Material.GOLD_BLOCK, 4));
				e.getPlayer().getWorld().dropItem(e.getClickedBlock().getLocation(), new ItemStack(Material.GOLDEN_APPLE, 15));
				e.getPlayer().getWorld().dropItem(e.getClickedBlock().getLocation(), new ItemStack(Material.DIAMOND, 35));
				
				ItemStack UberSword = new ItemStack(Material.DIAMOND_SWORD);
				ItemMeta UberSwordMeta = UberSword.getItemMeta();
				UberSwordMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&d[&5U&d] &5Epic sword of epic ness"));
				UberSword.setItemMeta(UberSwordMeta);
				UberSword.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 6);
				UberSword.addUnsafeEnchantment(Enchantment.DURABILITY, 3);
				UberSword.addUnsafeEnchantment(Enchantment.FIRE_ASPECT, 5);
				UberSword.addUnsafeEnchantment(Enchantment.DAMAGE_UNDEAD, 4);
				UberSword.addUnsafeEnchantment(Enchantment.DAMAGE_ARTHROPODS, 5);
				UberSword.addUnsafeEnchantment(Enchantment.LOOT_BONUS_MOBS, 5);
				
				ItemStack UberSword2 = new ItemStack(Material.DIAMOND_SWORD);
				ItemMeta UberSwordMeta2 = UberSword2.getItemMeta();
				UberSwordMeta2.setDisplayName(ChatColor.translateAlternateColorCodes('&', "&2[&aU&2] &aSword of dragons"));
				UberSword2.setItemMeta(UberSwordMeta2);
				UberSword2.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, 5);
				UberSword2.addUnsafeEnchantment(Enchantment.DURABILITY, 2);
				UberSword2.addUnsafeEnchantment(Enchantment.FIRE_ASPECT, 4);
				UberSword2.addUnsafeEnchantment(Enchantment.KNOCKBACK, 3);
				UberSword2.addUnsafeEnchantment(Enchantment.DAMAGE_UNDEAD, 5);
				UberSword2.addUnsafeEnchantment(Enchantment.DAMAGE_ARTHROPODS, 5);
				UberSword2.addUnsafeEnchantment(Enchantment.LOOT_BONUS_MOBS, 4);
				
				
				int Chance= rand.nextInt((100 - 0) + 1) + 0;
				if(Chance <= 1){
					int Chance2= rand.nextInt((100 - 0) + 1) + 0;
					if(Chance2 <= 50){
						e.getPlayer().getWorld().dropItem(e.getClickedBlock().getLocation(), UberSword);
					}else{
					e.getPlayer().getWorld().dropItem(e.getClickedBlock().getLocation(), UberSword2);
					}
					
				}
				
				


				 
				 
				 
				
				e.getClickedBlock().setType(Material.AIR);
				
			}
		}
		
	}


}
