package com.minewreck.mwessentials.redeems;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.minewreck.mwessentials.playerInfo.CustomConfig;

public class Redeemable {
	private ItemStack icon;
	private String name;
	private List<ItemStack> rewards;
	private Integer money;
	private Integer xp;

	public Redeemable(String name, ItemStack icon, List<ItemStack> rewards2, int money2, int xp2) {
		this.name = name;
		this.icon = icon;
		this.rewards = rewards2;
		this.money = money2;
		this.xp = xp2;
	}
	


	public String getSanatizedName(){
		return ChatColor.stripColor(getName()).replace(" ", "_").toLowerCase();
	}
	
	public boolean redeem(Player p){
		CustomConfig config = new CustomConfig(p.getUniqueId());
		if(config.getConfig().getStringList("redeems").contains(getSanatizedName())){
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&2MW &aRedeems&8] &2You have already done your redeems"));
			return false;
		}
		
		if(p.getInventory().firstEmpty() != -1){
			p.getInventory().addItem(icon);
			
			List<String> redeemed = config.getConfig().getStringList("redeems");
			redeemed.add(getSanatizedName());
			config.getConfig().set("redeems", redeemed);
			CustomConfig.saveConfig(p.getUniqueId());
			return true;
		}else{
			p.sendMessage("You didn't have enough room for the " + getName() + " item.");
			return false;
		}
	}

	public boolean fullredeem(Player p){
		int slots = 0;
		for(ItemStack i : p.getInventory()){
			if(i == null || i.getType() == Material.AIR){
				slots++;
			}
		}
		//should be good, try this.
		if(slots < rewards.size()){
			p.sendMessage("You need " + rewards.size() + " free slots to redeem your kit, please remove " + (rewards.size() - slots) + " items and try again!");
			return false;
		}
		for(ItemStack item : rewards){
			p.getInventory().addItem(new ItemStack[] {item});
		}
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "eco give "+p.getName()+" "+ money );
		p.giveExpLevels(xp);
		return true;	
	}
	
	public ItemStack getIcon() {
		return icon;
	}

	public void setIcon(ItemStack icon) {
		this.icon = icon;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<ItemStack> getRewards() {
		return rewards;
	}

	public void setRewards(List<ItemStack> rewards) {
		this.rewards = rewards;
	}

}
