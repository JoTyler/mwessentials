package com.minewreck.mwessentials.redeems;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import com.minewreck.mwessentials.Main;

public class RedeemCommand implements CommandExecutor, Listener {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("redeem")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
				for (Redeemable redeemable : Main.kits) {
					if (player.hasPermission("mwessentials.redeem."+ redeemable.getSanatizedName())) {
						redeemable.redeem(player);
						
					}
				}
				return true;
			}
			else {
				sender.sendMessage("You need to be a player to use this command!");
				return true;
			}
		}
		return false;
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent event) {
		if(event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR){
			if (event.getItem() != null && event.getItem().hasItemMeta()) {
				for (Redeemable redeemable : Main.kits) {
					if (redeemable.getIcon().getType() == event.getItem().getType()) {
						if (event.getItem().getItemMeta()
								.equals(redeemable.getIcon().getItemMeta())) {
							event.setCancelled(true);
							event.getPlayer().updateInventory();
							if (redeemable.fullredeem(event.getPlayer())) {
								event.getPlayer().setItemInHand(null);
							}

						}
					}
				}
			}
		}
	}
}