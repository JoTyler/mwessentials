package com.minewreck.mwessentials.teams;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Location;


public class TeamData {

	public static HashMap<String, TeamData> data = new HashMap<String, TeamData>();

	protected String leader, world, name;
	protected ArrayList<String> members, mods;
	protected Integer teamhome_x, teamhome_y, teamhome_z;
	protected boolean homeset;

	public TeamData(String TeamName) {
		TeamConfig config = new TeamConfig(TeamName);
		this.name = config.getConfig().getString(TeamName);
		this.members = (ArrayList<String>) config.getConfig().getStringList(TeamName + ".members");
		this.leader = config.getConfig().getString(TeamName + ".leader");
		this.teamhome_x = config.getConfig().getInt(TeamName + ".Home.X");
		this.teamhome_y = config.getConfig().getInt(TeamName + ".Home.Y");
		this.teamhome_z = config.getConfig().getInt(TeamName + ".Home.Z");
		this.homeset = config.getConfig().getBoolean(TeamName + ".Home.Set");
		this.world = config.getConfig().getString(TeamName + ".Home.World");
		this.mods = (ArrayList<String>) config.getConfig().getStringList(TeamName + ".mods");
	}
	
	public ArrayList<String> Members(){
		return members;
	}
	public ArrayList<String> Mods(){
        return mods;
	}
	public boolean HomeSet(){
		return homeset;
	}
	public String Leader(){
		return leader;
	}
	public Location TeamHome(){
	return new Location(Bukkit.getWorld(this.world), this.teamhome_x, this.teamhome_y, this.teamhome_z);	
	}
	public String TeamName(){
		return name;
	}

	
}
