package com.minewreck.mwessentials.teams;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.minewreck.mwessentials.Main;
import com.minewreck.mwessentials.playerInfo.CustomConfig;
import com.minewreck.mwessentials.playerInfo.PlayerData;



public class TeamCommand implements CommandExecutor {
	
	String prefix = ChatColor.translateAlternateColorCodes('&', "&8[&2MW &aTeams&8]");

	HashMap<String, String> invite = new HashMap<String, String>();
	
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		final Player player = (Player) sender;
		if(args.length < 1){
			TeamHelp(player);
			return true;
		}
		else if(args[0].equalsIgnoreCase("create")){
			if(args.length < 1){
				TeamHelp(player);
				return true;
			}
			if(PlayerData.data.get(player).on_Team()){
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + " &2You have already made a team or are on one!"));
			}else{
				if(TeamConfig.configExists(args[1])){
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + " &2Sorry! That team exists!"));
				}else{
			
			TeamConfig config = new TeamConfig(args[1]);
			String TeamName = args[1];
			config.getConfig().set(TeamName + ".leader", sender.getName());
			ArrayList<String> temp = new ArrayList<String>();
			ArrayList<String> tempmod = new ArrayList<String>();
			
			temp.add(sender.getName());
			config.getConfig().set(TeamName + ".members", temp);
			config.getConfig().set(TeamName + ".Home.X", 0);
			config.getConfig().set(TeamName + ".Home.Y", 0);
			config.getConfig().set(TeamName + ".Home.Z", 0);
			config.getConfig().set(TeamName + ".Home.Set", false);
			config.getConfig().set(TeamName + ".Home.World", "");
			config.getConfig().set(TeamName + ".mods", tempmod);
			TeamConfig.saveConfig(TeamName);
			TeamData.data.put(TeamName, new TeamData(TeamName));
			
			CustomConfig config2 = new CustomConfig(player.getUniqueId());
			config2.getConfig().set("Team", true);
			config2.getConfig().set("TeamName", TeamName);
		    CustomConfig.saveConfig(player.getUniqueId());
		    
		    PlayerData.data.put(player, new PlayerData(player));
		    player.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + "&2You have created the team &a" + TeamName));
			}
			}
			
		}
		else if(args[0].equalsIgnoreCase("info")){
			if(args.length < 2){
				if(PlayerData.data.get(player).on_Team()){
				String TeamName = PlayerData.data.get(player).Team_Name();
				TeamInfo(TeamName, player);
				return true;
				}else{
					
				    player.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + "&2That player is not on a team"));

				}
			}else{
			if(TeamConfig.configExists(args[1])){
				String TeamName = args[1];
			TeamInfo(TeamName, player);
			return true;
			}
			else if(!(Bukkit.getPlayer(args[1]) == null)){
				Player TPlayer = Bukkit.getPlayer(args[1]);
			if(PlayerData.data.get(TPlayer).on_Team()){
				String TeamName = PlayerData.data.get(TPlayer).Team_Name();
			TeamInfo(TeamName, player);
			return true;
			}else{
			    player.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + "&2That player is not on a team"));

			}
		}else{
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + " &2That player or team is not found!"));
			}
		}	
	}
		
		else if(args[0].equalsIgnoreCase("invite")){
			if(PlayerData.data.get(player).on_Team()){
				String TeamName = PlayerData.data.get(player).Team_Name();
				if(IsLeader(TeamName, player) || IsMod(TeamName, player)){
			final String target = args[1];
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + " &2You have invited "+target + " to the team."));
			if(!(Bukkit.getPlayer(target) == null) || Bukkit.getPlayer(target).isOnline()){
			Bukkit.getPlayer(target).sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + " &2You have been invited to &a" + TeamName));
			invite.put(target, TeamName);
			new BukkitRunnable() {

				@Override
				public void run() {
					invite.remove(target);

				}
			}.runTaskLater(Main.getInstance(), 120*20);
			
			
			
				}else{
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + " &2That player is not online or is not found!"));
				}
				}
			}else{
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + "&2That player is not on a team!"));

			}
		}
		else if(args[0].equalsIgnoreCase("join")){
			if(invite.containsKey(sender.getName())){
				if(PlayerData.data.get(player).on_Team()){
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + " &2You are already on a team! Please type /TEAM LEAVE to leave the team!"));
				}else{
				String InvitedTeam = invite.get(sender.getName());
				ArrayList<String> temp = new ArrayList<String>();
				temp.addAll(TeamData.data.get(InvitedTeam).Members());
				temp.add(sender.getName());
				TeamConfig config = new TeamConfig(InvitedTeam);
				config.getConfig().set(InvitedTeam + ".members", temp);
				TeamConfig.saveConfig(InvitedTeam);
				TeamData.data.put(InvitedTeam, new TeamData(InvitedTeam));
				
				CustomConfig config2 = new CustomConfig(player.getUniqueId());
				config2.getConfig().set("Team", true);
				config2.getConfig().set("TeamName", InvitedTeam);
			    CustomConfig.saveConfig(player.getUniqueId());
			    PlayerData.data.put(player, new PlayerData(player));
			    invite.remove(sender.getName());
			    player.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + " &2You have joined &a" + InvitedTeam));
				
				}
				
				
				
				
			}else{
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + " &2You need to be invited to join a team!"));
			}
		}
		else if(args[0].equalsIgnoreCase("sethome")){
			if(PlayerData.data.get(player).on_Team()){
				String TeamName = PlayerData.data.get(player).Team_Name();
				if(IsLeader(TeamName, player)){
	
			TeamConfig config = new TeamConfig(TeamName);
			int x = (int) player.getLocation().getX();
			int y = (int) player.getLocation().getY();
			int z = (int) player.getLocation().getZ();
		    String world = player.getWorld().getName();
			config.getConfig().set(TeamName + ".Home.X", x);
			config.getConfig().set(TeamName + ".Home.Y", y);
			config.getConfig().set(TeamName + ".Home.Z", z);
			config.getConfig().set(TeamName + ".Home.World", world);
			config.getConfig().set(TeamName + ".Home.Set", true);
			TeamConfig.saveConfig(TeamName);
			TeamData.data.put(TeamName, new TeamData(TeamName));
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + " &2You have set the teams home!"));
				}
			}
				
		}
	
		else if(args[0].equalsIgnoreCase("home")){
			if(PlayerData.data.get(player).on_Team()){
				final String TeamName = PlayerData.data.get(player).Team_Name();
				if(TeamData.data.get(TeamName).HomeSet()){
					player.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + " &2You will be teleported to your team home in &a 7 SECONDS&2!"));
					new BukkitRunnable() {

						@Override
						public void run() {
							player.teleport(TeamData.data.get(TeamName).TeamHome());

						}
					}.runTaskLater(Main.getInstance(), 7*20);
				}
			}else{
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + " &2You don't have a team!"));
			}
				
		}

		
		else if(args[0].equalsIgnoreCase("mod") || args[0].equalsIgnoreCase("promote")){
			if(PlayerData.data.get(player).on_Team()){
				String TeamName = PlayerData.data.get(player).Team_Name();
				if(IsLeader(TeamName, player)){
					String target = args[1];
					if(TeamData.data.get(TeamName).Members().contains(target)){
				
				TeamConfig config = new TeamConfig(TeamName);
				ArrayList<String> tempmod = new ArrayList<String>();
				tempmod.addAll(TeamData.data.get(TeamName).Mods());
				tempmod.add(target);
				config.getConfig().set(TeamName + ".mods", tempmod);
				TeamConfig.saveConfig(TeamName);
				TeamData.data.put(TeamName, new TeamData(TeamName));
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + " &2You have promoted &a" + target + "&2to moderator!"));

					}
				if(Bukkit.getPlayer(target) == null){
	return false;
				}else{
					Bukkit.getPlayer(target).sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + " &2You have been promoted to moderator"));
				}
				
				}
				
			}

		}
		else if(args[0].equalsIgnoreCase("demote") || args[0].equalsIgnoreCase("unmod")){
			if(PlayerData.data.get(player).on_Team()){
			if(IsLeader(PlayerData.data.get(player).Team_Name(), player)){
				Player TargetMod = Bukkit.getPlayer(args[1]);
				if(IsMod(PlayerData.data.get(player).Team_Name(), TargetMod)){
				
      	
			TargetMod.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + "&2You have been demoted to member!"));
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + "&2You have demoted &a" + TargetMod.getName()));
          DemoteMod(player, args[1]);
				}
			}
			}else{
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + "&2You are not on a team!"));
			}
		}
		else if(args[0].equalsIgnoreCase("kick")){
			if(PlayerData.data.get(player).on_Team()){
				 String TeamName = PlayerData.data.get(player).Team_Name();
				if(IsLeader(TeamName, player) || IsMod(TeamName, player)){
                   String target = args[1];
                   Player TPlayer = Bukkit.getPlayer(target);
           
   				ArrayList<String> temp = new ArrayList<String>();
   				temp.addAll(TeamData.data.get(TeamName).Members());
   				temp.remove(target);
   				TeamConfig config = new TeamConfig(TeamName);
   				config.getConfig().set(TeamName + ".members", temp);
   				TeamConfig.saveConfig(TeamName);
   				TeamData.data.put(TeamName, new TeamData(TeamName));
   				
   				AssignTeam(TPlayer, false, "");
  				DemoteMod(player, target);
  				
  				player.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + "&2You have kicked &a" + TPlayer.getName() + "&2from the team!"));
  				TPlayer.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + "&2You have been kicked from the team!"));
				}				
			}
		}
		else if(args[0].equalsIgnoreCase("leave")){
			String TeamName = PlayerData.data.get(player).Team_Name();
			if(IsLeader(TeamName, player)){
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + "&2You must first disband your team!"));
			}else{
			ArrayList<String> temp = new ArrayList<String>();
			temp.addAll(TeamData.data.get(TeamName).Members());
			temp.remove(player.getName());
			TeamConfig config = new TeamConfig(TeamName);
			config.getConfig().set(TeamName + ".members", temp);
			TeamConfig.saveConfig(TeamName);
			TeamData.data.put(TeamName, new TeamData(TeamName));
			
			AssignTeam(player, false, "");
			DemoteMod(player, player.getName());
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + "&2You have leaft the team!"));
			}
		}
		else if(args[0].equalsIgnoreCase("disband")){
			if(OnTeam(player)){
				
				String TeamName = PlayerData.data.get(player).Team_Name();
				
		if(IsLeader(TeamName, player)){
			player.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + "&2You have disbanded the team!"));
			for (String member : TeamData.data.get(TeamName).Members()){
				Player target = Bukkit.getPlayer(member);
				AssignTeam(target, false, "");
				player.sendMessage(ChatColor.translateAlternateColorCodes('&', prefix + "&2You have been kicked from the team because the leader disbaned the team!"));
			}
			TeamConfig.deleteConfig(TeamName);
			TeamData.data.remove(TeamName);
		}
		}
		}else{
			TeamHelp(player);
		}
	
		return false;
	}
	public boolean IsLeader(String team, Player check){
		if(TeamData.data.get(team).Leader().equalsIgnoreCase(check.getName())){
			return true;
		}else{
		return false;
		}
	}
    public boolean IsMod(String team, Player check){
    	if(TeamData.data.get(team).Mods().contains(check.getName())){
    		return true;}else{
    	return false;
    		}
    }
	public boolean OnTeam(Player check){
		if(PlayerData.data.get(check).on_Team()){return true;}
	else{
		return false;}
	}
	public void TeamInfo(String Team, Player player){
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8-------------------------------"));
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&2Team name: &a" + Team));
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&2Leader: &a" + TeamData.data.get(Team).Leader()));
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&2Mods: &a" + TeamData.data.get(Team).Mods()));
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&2Members: &a" + TeamData.data.get(Team).Members()));
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8-------------------------------"));
	}
    public void AssignTeam(Player player, Boolean value, String Team){
    	CustomConfig config = new CustomConfig(player.getUniqueId());
    	config.getConfig().set("Team", value);
    	config.getConfig().set("TeamName", Team);
    	CustomConfig.saveConfig(player.getUniqueId());
    	if(player.isOnline()){
    		PlayerData.data.put(player, new PlayerData(player));
    	}
    	
    }
	public void TeamHelp(Player player){
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8-------------------------------"));
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&2Leader Commands:"));
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', "  &2/&ateam promote &2[&aPLAYER&2]"));
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', "  &2/&ateam demote &2[&aPLAYER&2]"));
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', "  &2/&ateam disband"));
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', "  &2/&ateam sethome"));
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', "  &2/&ateam create &2[&aNAME&2]"));
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', "  &2/&ateam leave"));
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&2Moderator Commands:"));
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', "  &2/&ateam kick &2[&aPLAYER&2]"));
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', "  &2/&ateam invite"));
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&2Member Commands:"));
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', "  &2/&ateam leave"));
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', "  &2/&ateam join &2[&aNAME&2]"));
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', "  &2/&ateam info &2[&aNAME&2]"));
		player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8-------------------------------"));
	}
	public void DemoteMod(Player player, String target){
		
		if(PlayerData.data.get(player).on_Team()){
			if(TeamData.data.get(PlayerData.data.get(player).Team_Name()).Leader().equalsIgnoreCase(player.getName())){
			String TeamName = PlayerData.data.get(player).Team_Name();
			TeamConfig config = new TeamConfig(TeamName);
			ArrayList<String> tempmod = new ArrayList<String>();
			tempmod.addAll(TeamData.data.get(TeamName).Mods());
			tempmod.remove(target);
			config.getConfig().set(TeamName + ".mods", tempmod);
			TeamConfig.saveConfig(TeamName);
			TeamData.data.put(TeamName, new TeamData(TeamName));
			}else if(TeamData.data.get(PlayerData.data.get(player).Team_Name()).Mods().contains(target)) {
				String TeamName = PlayerData.data.get(player).Team_Name();
				TeamConfig config = new TeamConfig(TeamName);
				ArrayList<String> tempmod = new ArrayList<String>();
				tempmod.addAll(TeamData.data.get(TeamName).Mods());
				tempmod.remove(target);
				config.getConfig().set(TeamName + ".mods", tempmod);
				TeamConfig.saveConfig(TeamName);
				TeamData.data.put(TeamName, new TeamData(TeamName));
				
				
				
				
				
			}
		}

	}

}
