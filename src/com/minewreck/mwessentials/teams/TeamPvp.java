package com.minewreck.mwessentials.teams;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import com.minewreck.mwessentials.playerInfo.PlayerData;

public class TeamPvp implements Listener{

	
	@EventHandler
	public void onEntityDamageEvent(EntityDamageByEntityEvent e) {
		if (e.getDamager() instanceof Player && e.getEntity() instanceof Player) {
			if(PlayerData.data.get(e.getEntity()).on_Team() && PlayerData.data.get(e.getDamager()).on_Team()){
	
		 Player hit = (Player) e.getEntity();
	     Player hitter = (Player) e.getDamager();
	     String TeamHit = PlayerData.data.get(hit).Team_Name();
	     String TeamHitter = PlayerData.data.get(hitter).Team_Name();
	    if(TeamHit.equalsIgnoreCase(TeamHitter)){
	    	e.setCancelled(true);
	    	hitter.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&2MW &a Teams&8]&2 That player is in your team!"));
	    
	    }else{return;}
			}else{return;}
	   


			
			
		}
		if(e.getDamager() instanceof Projectile && e.getEntity() instanceof Player){
			Projectile p = (Projectile) e.getDamager();
			if(p.getShooter() instanceof Player){
				if(PlayerData.data.get(e.getEntity()).on_Team() && PlayerData.data.get((Player)p.getShooter()).on_Team()){
					Player hit = (Player) e.getEntity();
				     Player hitter = (Player) p.getShooter();
				     String TeamHit = PlayerData.data.get(hit).Team_Name();
				     String TeamHitter = PlayerData.data.get(hitter).Team_Name();
				    if(TeamHit.equalsIgnoreCase(TeamHitter)){
				    	e.setCancelled(true);
				    	hitter.sendMessage("" + hit.getName() + " is on your team.");
				    
				    }else{return;}
					
					
					
					
				}
			}
		}
	}
}
