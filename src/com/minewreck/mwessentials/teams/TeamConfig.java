package com.minewreck.mwessentials.teams;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.YamlConfiguration;

import com.minewreck.mwessentials.Main;

public class TeamConfig {

	private static YamlConfiguration config;
	static String dir = "/teamFiles/";
	public TeamConfig(String id) {
		if(configExists(id)) {
			config = getConfig(id);
		} else {
			config = createConfig(id);
		}
	}
	
	public YamlConfiguration getConfig() {
		return config;
	}
	
	public static boolean configExists(String id) {
		return new File(Main.getInstance().getDataFolder() + dir + id+ ".yml").exists();
	}
	
	public static YamlConfiguration createConfig(String id) {
		File file = new File(Main.getInstance().getDataFolder() + dir + id + ".yml");
		return YamlConfiguration.loadConfiguration(file);
	}
	public static void deleteConfig(String id){
File file = new File(Main.getInstance().getDataFolder() + dir + id + ".yml");
file.delete();

	}
	public static YamlConfiguration getConfig(String id) {
		File file = new File(Main.getInstance().getDataFolder() + dir + id + ".yml");
		return YamlConfiguration.loadConfiguration(file);
	}
	
	public static void saveConfig(String id) {
		File file = new File(Main.getInstance().getDataFolder() + dir + id+ ".yml");
		try {
			config.save(file);
			return;
		} catch (IOException e) {
			System.out.println("CANNOT SAVE FILE: " + file);
			e.printStackTrace();
			return;
		}
	}
}
