package com.minewreck.mwessentials.voting;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Rewards implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2,
			String[] arg3) {
		
		Player player = (Player) sender;
		 player.sendMessage(ChatColor.DARK_GRAY + "-----------------------------------------------------");
		 player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&2Vote for epic ingame items, thos items are:"));
		 player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&2$2,500"));
		 player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aDiamond Helmet &2(Enchants: &aProtection&2:&a3&2, &aUnbreaking&2:&a2&2"));
		 player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aDiamond ChestPlate &2(Enchants: &aProtection&2:&a3&2, &aUnbreaking&2:&a2&2"));
		 player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aDiamond Leggings &2(Enchants: &aProtection&2:&a3&2, &aUnbreaking&2:&a2&2"));
		 player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aDiamond Boots &2(Enchants: &aProtection&2:&a3&2, &aUnbreaking&2:&a2&2"));
		 player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&aDiamond Sword &2()Enchants: &aSharpness:2 Unbreaking:2&2"));
		 player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&a2 God Apples&2(&aNotch Apples&2"));
		 player.sendMessage(ChatColor.DARK_GRAY +"-----------------------------------------------------");
		 player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&2Voting links"));
		 player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&ahttp://bit.do/MWVote1"));
		 player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&ahttp://bit.do/MWVote2"));
		 player.sendMessage(ChatColor.translateAlternateColorCodes('&', "&ahttp://bit.do/MWVote3"));
		 player.sendMessage(ChatColor.DARK_GRAY + "-----------------------------------------------------");
		
		return false;
	}
	
	
	
	
}
