package com.minewreck.mwessentials.voting;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.vexsoftware.votifier.model.VotifierEvent;

public class VoteListener implements Listener {

	
	
	
	@EventHandler
	public void VoteEvent(VotifierEvent e){
		Player voter = Bukkit.getPlayer(e.getVote().getUsername());
		if(voter.isOnline()){
		Reward(voter);
		
		Bukkit.getServer().broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&8[&2MW &aVote&8] &a"+voter.getName() +"&2 Has voted for the server /Vote!!"));
		}
		
	}
	
	@SuppressWarnings("deprecation")
	public void Reward(Player p){
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "eco give "+p.getName()+" "+ 2500);
		ItemStack Helm2 = new ItemStack(Material.DIAMOND_HELMET, 1);
		ItemStack Chest2 = new ItemStack(Material.DIAMOND_CHESTPLATE, 1);
		ItemStack Leg2 = new ItemStack(Material.DIAMOND_LEGGINGS, 1);
		ItemStack Boot2 = new ItemStack(Material.DIAMOND_BOOTS, 1);
		ItemMeta name2 = Helm2.getItemMeta();
		
		name2.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3, true);
		name2.addEnchant(Enchantment.DURABILITY, 2, true);
		Helm2.setItemMeta(name2);
		Chest2.setItemMeta(name2);
		Leg2.setItemMeta(name2);
		Boot2.setItemMeta(name2);
		
		ItemStack Sword2 = new ItemStack(Material.DIAMOND_SWORD);
		ItemMeta SMeta = Sword2.getItemMeta();
		SMeta.addEnchant(Enchantment.DAMAGE_ALL, 2, true);
		SMeta.addEnchant(Enchantment.DURABILITY, 2, true);
		Sword2.setItemMeta(SMeta);
		
		short id = 1;
		ItemStack Apple2 = new ItemStack(Material.getMaterial(322), 2, id);
		
		p.getInventory().addItem(Helm2);
		p.getInventory().addItem(Chest2);
		p.getInventory().addItem(Leg2);
		p.getInventory().addItem(Boot2);
		p.getInventory().addItem(Sword2);
		p.getInventory().addItem(Apple2);
		
		
		}
	
}
