package com.minewreck.mwessentials;


import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;


public class JoinMessage implements Listener {

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {
		e.setJoinMessage("");
		int playercount = Bukkit.getServer().getOnlinePlayers().size();
		int onlineStaff = 0;
		for (Player p : Bukkit.getOnlinePlayers()) {
			if (p.hasPermission("mwessentials.list.staff")) {
				onlineStaff++;
			}
		}
		e.getPlayer().sendMessage(
				ChatColor.translateAlternateColorCodes('&',
						"&8--------------------------------------------"));
		e.getPlayer().sendMessage(
				ChatColor.translateAlternateColorCodes('&',
						"                 &2MineWreck         "));
		e.getPlayer().sendMessage(
				ChatColor.translateAlternateColorCodes('&',
						"                 &aOverkill          "));
		e.getPlayer().sendMessage(
				ChatColor.translateAlternateColorCodes('&',
						"       &2A over-powered raiding server!"));
		e.getPlayer().sendMessage(
				ChatColor.translateAlternateColorCodes('&',
						"   &2There are currently &a" + playercount + "&2/&a"
								+ Bukkit.getMaxPlayers()
								+ " &2online &a" + onlineStaff
								+ " &2Moderators online right now!"));
		e.getPlayer().sendMessage(
				ChatColor.translateAlternateColorCodes('&',
						"      &7By playing on this server you agree to"));
		e.getPlayer().sendMessage(
				ChatColor.translateAlternateColorCodes('&',
						"    &7both /rules and the Terms at minewreck.com"));
		e.getPlayer().sendMessage(
				ChatColor.translateAlternateColorCodes('&',
						"&8--------------------------------------------"));
		
		/*
 PacketContainer pc = Main.protocalManager.createPacket(PacketType.Play.Server.PLAYER_LIST_HEADER_FOOTER);
	     
	     pc.getChatComponents().write(0, WrappedChatComponent.fromText(ChatColor.LIGHT_PURPLE	 + "Welcome to" + ChatColor.GOLD + " MineWreck.com"))
	     .write(1, WrappedChatComponent.fromText(ChatColor.RED + "Our Website:"+ChatColor.GREEN+ " MineWreck.com"));
	     
	     
	     
	     try
	     {
	    	 Main.protocalManager.sendServerPacket(e.getPlayer(), pc);
	    	 
	    	 
	     }catch(Exception ex){
	    	 ex.printStackTrace();
	     }
		 
	*/	
	}
}