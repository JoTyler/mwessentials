package com.minewreck.mwessentials.punnishments;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import com.minewreck.mwessentials.Main;
import com.minewreck.mwessentials.playerInfo.CustomConfig;
import com.minewreck.mwessentials.playerInfo.PlayerData;

public class BanCommand implements CommandExecutor, Listener {
	public Main main;

	/**
	 * permissions: MWBanManager.ban commands: /ban [player] [reason]
	 */

	@SuppressWarnings("static-access")
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e) {
		PlayerData.data.put(e.getPlayer(), new PlayerData(e.getPlayer()));
		Player p = e.getPlayer();
		PlayerData pd = new PlayerData(p);
		
		if (pd.data.get(p).is_Banned()) {
			String message = ChatColor
					.translateAlternateColorCodes(
							'&',
							"&8[&2MW &aBanManager&8] &2You have been banned!\n "
									+ "&2Make an appeal at &ahttp://www.minewreck.com/ "+
									pd.data.get(p).ban_Reason());
			p.kickPlayer(message);
		}

		return;
	}



	@SuppressWarnings({ "static-access", "deprecation" })
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		Player p = (Player) sender;

		if (!(p.hasPermission("mwbanmanager.ban"))) {
			p.sendMessage(ChatColor.translateAlternateColorCodes('&',
					"&8[&2MW &aBanManager&8] &4You don't have permission!"));
		}
		else {
			if (args.length == 0) {
				p.sendMessage(ChatColor
						.translateAlternateColorCodes('&',
								"&8[&2MW &aBanManager&8] &2/&aban &2[&aPLAYER&2] &2[&aREASON&2]"));
			}
			else if (args.length >= 1) {
				OfflinePlayer target = Bukkit.getOfflinePlayer(args[0]);
				if (target == null) {
					p.sendMessage(ChatColor
							.translateAlternateColorCodes('&',
									"&8[&2MW &aBanManager&8] &2That player doesn't exist or is not online!"));
					return true;

				}
				else {
					String message = ChatColor
							.translateAlternateColorCodes(
									'&',
									"&8[&2MW &aBanManager&8] &2You have been banned!\n "
											+ "&2Make an appeal at &ahttp://www.minewreck.com/");
					StringBuilder sbMsg = new StringBuilder();
					for (int i = 1; i < args.length; i++) {
						message = message
								+ ChatColor.translateAlternateColorCodes('&',
										args[i]) + " ";
						sbMsg.append(ChatColor.translateAlternateColorCodes(
								'&', args[i]) + " ");
					}

				
					p.sendMessage(ChatColor.translateAlternateColorCodes('&',
							"&8[&2MW &aBanManager&8] &2You have banned &a"
									+ target.getName() + " &2for "
									+ sbMsg.toString() + "&2!"));
					
					CustomConfig config = new CustomConfig(target.getUniqueId());
					config.getConfig().set("Is-banned", true);
					config.getConfig().set("banreason", sbMsg.toString());
					config.saveConfig(target.getUniqueId());
				
					if(target.isOnline()){
						Player playerT = (Player) target;
						PlayerData.data.put(playerT, new PlayerData(playerT));
						playerT.kickPlayer(message + sbMsg.toString()); 
						
						
					}

				}
			}
		}

		return false;
	}
}