package com.minewreck.mwessentials.punnishments;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.minewreck.mwessentials.playerInfo.CustomConfig;
import com.minewreck.mwessentials.playerInfo.PlayerData;

public class WarnCommand implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if(sender.hasPermission("MWessentials.warn")){
	if(args.length < 1){
		sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&2MW &aBanManager&8] &2/&awarn  &2[&aPLAYER&8] &2[&aREASON&2]"));
return true;
	}
		if(!(Bukkit.getPlayer(args[0])== null)){
			Player target = Bukkit.getPlayer(args[0]);
			CustomConfig config = new CustomConfig(target.getUniqueId());
			int Warns = PlayerData.data.get(target).Warns() + 1;
			config.getConfig().set("Warns", Warns);
			CustomConfig.saveConfig(target.getUniqueId());
			PlayerData.data.put(target, new PlayerData(target));
			
			StringBuilder sbMsg = new StringBuilder();
			for (int i = 1; i < args.length; i++) {
				sbMsg.append(ChatColor.translateAlternateColorCodes(
						'&', args[i]) + " ");
			}
			Bukkit.getServer().broadcastMessage(ChatColor.translateAlternateColorCodes('&', "&8[&2MW &aBanManager&8] &a"+ args[0] +" &2has been warned for &a"+sbMsg.toString() +"&2!"));
					}else{
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&2MW &aBanManager&8] &2/&awarn  &2[&aPLAYER&8] &2[&aREASON&2]"));
		}
		}else{
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
					"&8[&2MW &aBanManager&8] &4You don't have permission!"));
		}
		return false;
	}

}
