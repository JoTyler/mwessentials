package com.minewreck.mwessentials.punnishments;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.minewreck.mwessentials.Main;

public class KickCommand implements CommandExecutor {
	public Main main;

	/**
	 * permissions: MWBanManager.ban
	 * commands: /kick [player] [reason]
	 */
    
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		Player p = (Player) sender;
		
		if (!(p.hasPermission("mwbanmanager.kick"))) {
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&2MW &aBanManager&8] &4You don't have permission!"));
		} else {
			if (args.length == 0) {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&2MW &aBanManager&8] &2/&akick &2[&aPLAYER&2] &2[&aREASON&2]"));
			} else if (args.length >= 1){
				Player target = Bukkit.getPlayer(args[0]);
				if (target == null) {
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&2MW &aBanManager&8] &2That player doesn't exist or is not online!"));
					return true;
					
				} else {
					String message = ChatColor.translateAlternateColorCodes('&', "&8[&2MW &aBanManager&8] &2You have been kicked! \n");
					for(int i = 1; i < args.length; i++) {
						message = message + ChatColor.translateAlternateColorCodes('&', args[i])+ " ";
					}
					target.kickPlayer(message);     
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&2MW &aBanManager&8] &2You have kicked &a"+ target.getName() +"&2!"));
				}
			}
		}

		return false;
	}
}