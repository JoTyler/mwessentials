package com.minewreck.mwessentials.punnishments;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import com.minewreck.mwessentials.Main;
import com.minewreck.mwessentials.playerInfo.CustomConfig;
import com.minewreck.mwessentials.playerInfo.PlayerData;

public class UnMuteCommand implements CommandExecutor, Listener {

	public Main main;


	/**
	 * permissions: MWBanManager.mute commands: /mute [player]
	 */

	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		Player p = (Player) sender;

		if (!(p.hasPermission("mwbanmanager.unmute"))) {
			p.sendMessage(ChatColor.translateAlternateColorCodes('&',
					"&8[&2MW &aBanManager&8] &4You don't have permission!"));
		}
		else {
			if (args.length == 0) {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&',
						"&8[&2MW &aBanManager&8] &2/&aunmute &2[&aPLAYER&2]"));
			}
			else if (args.length >= 1) {
				Player target = Bukkit.getPlayer(args[0]);
				if (target == null) {
					p.sendMessage(ChatColor
							.translateAlternateColorCodes('&',
									"&8[&2MW &aBanManager&8] &2That player doesn't exist or is not online!"));
					return true;

				}
				else {
					String message = ChatColor.translateAlternateColorCodes(
							'&',
							"&8[&2MW &aBanManager&8] &2You have been un-muted!");
					for (int i = 1; i < args.length; i++) {
						message = message
								+ ChatColor.translateAlternateColorCodes('&',
										args[i]) + " ";
					}
					if (target.isOnline()) {
						target.sendMessage(message);
						CustomConfig config = new CustomConfig(target.getUniqueId());
						config.getConfig().set("Is-muted", false);
						CustomConfig.saveConfig(target.getUniqueId());
						PlayerData.data.put(target, new PlayerData(target));

					}
				}
			}
		}

		return false;
	}
}