package com.minewreck.mwessentials.punnishments;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import com.minewreck.mwessentials.Main;
import com.minewreck.mwessentials.playerInfo.CustomConfig;


public class UnbanCommand implements CommandExecutor, Listener {
	public Main main;

	/**
	 * permissions: MWBanManager.ban
	 * commands: /unban [player]
	 */
	
    
    @SuppressWarnings({ "deprecation", "static-access" })
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(!(sender instanceof Player)){
			OfflinePlayer target = Bukkit.getOfflinePlayer(args[0]);
			CustomConfig config = new CustomConfig(target.getUniqueId());
			config.getConfig().set("Is-banned", false);
			config.getConfig().set("banreason", "none");
			config.saveConfig(target.getUniqueId());
			System.out.println("You have unbanned "+ args[0]);


			return true;
		}
		Player p = (Player) sender;
		if (!(p.hasPermission("mwbanmanager.ban"))) {
			p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&2MW &aBanManager&8] &4You don't have permission!"));
		} else {
			if (args.length == 0) {
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&2MW &aBanManager&8] &2/&aunban &2[&aPLAYER&2]"));
			} else if (args.length >= 1){
				OfflinePlayer target = Bukkit.getOfflinePlayer(args[0]);
				if (target == null) {
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&2MW &aBanManager&8] &2That player doesn't exist or is not online!"));
					return true;
					
				} else {
					String message = "";
					for(int i = 1; i < args.length; i++) {
						message = message + ChatColor.translateAlternateColorCodes('&', args[i])+ " ";
					}	 
					CustomConfig config = new CustomConfig(target.getUniqueId());
					config.getConfig().set("Is-banned", false);
					config.getConfig().set("banreason", "none");
					config.saveConfig(target.getUniqueId());
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&2MW &aBanManager&8] &2You have unbanned "+ target.getName() +"!"));

      
			     	
				}
			}
		}

		return false;
	}
}