package com.minewreck.mwessentials.playerInfo;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.bukkit.configuration.file.YamlConfiguration;

import com.minewreck.mwessentials.Main;

public class CustomConfig {

	private static YamlConfiguration config;
	
	public CustomConfig(UUID id) {
		if(configExists(id)) {
			config = getConfig(id);
		} else {
			config = createConfig(id);
		}
	}
	
	public YamlConfiguration getConfig() {
		return config;
	}
	
	public boolean configExists(UUID id) {
		return new File(Main.getInstance().getDataFolder() + "/playerFiles/" + id.toString() + ".yml").exists();
	}
	
	public static YamlConfiguration createConfig(UUID id) {
		File file = new File(Main.getInstance().getDataFolder() + "/playerFiles/" + id.toString() + ".yml");
		return YamlConfiguration.loadConfiguration(file);
	}
	
	public static YamlConfiguration getConfig(UUID id) {
		File file = new File(Main.getInstance().getDataFolder() + "/playerFiles/" + id.toString() + ".yml");
		return YamlConfiguration.loadConfiguration(file);
	}
	
	public static void saveConfig(UUID id) {
		File file = new File(Main.getInstance().getDataFolder() + "/playerFiles/" + id.toString() + ".yml");
		try {
			config.save(file);
			return;
		} catch (IOException e) {
			System.out.println("CANNOT SAVE FILE: " + file);
			e.printStackTrace();
			return;
		}
	}
}
