package com.minewreck.mwessentials.playerInfo;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class InfoCommand implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		if(args.length < 1){
			sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&2MW &aBanManager&8] &2/&ainfo &2[&aPLAYER&2]"));
		}else{
			String target = args[0];
			if(!(Bukkit.getPlayer(target) == null)){
				Player target2 = Bukkit.getPlayer(target);
		sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8&m-------------------------------------"));
		sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&2Info for &a" + target));
		sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&2Banned: &a"+ PlayerData.data.get(target2).is_Banned()));
		sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&2Ban Reason: &a" + PlayerData.data.get(target2).ban_Reason()));
		sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&2Muted: &a"+ PlayerData.data.get(target2).is_Muted()));
		sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&2Time Muted: &a(NOT ADDED YET)"));
		sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&2Team: &a" + PlayerData.data.get(target2).Team_Name()));
		sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&2Warns: &a" + PlayerData.data.get(target2).Warns()));
		sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8&m-------------------------------------"));
			}else{
				sender.sendMessage(ChatColor.translateAlternateColorCodes('&', "&8[&2MW &aBanManager&8] &2/&ainfo &2[&aPLAYER&2]"));
			}
		}
		
		return false;
	}

	
	
	
	
}
