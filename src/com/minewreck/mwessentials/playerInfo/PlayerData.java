package com.minewreck.mwessentials.playerInfo;

import java.util.HashMap;

import org.bukkit.entity.Player;

public class PlayerData {

	public static HashMap<Player, PlayerData> data = new HashMap<Player, PlayerData>();

	protected boolean is_muted, is_banned, on_team;
	protected String ban_reason, team_name;
	protected int mute_time, warns;

	public PlayerData(Player player) {
		CustomConfig config = new CustomConfig(player.getUniqueId());
		this.is_banned = config.getConfig().getBoolean("Is-banned");
		this.ban_reason = config.getConfig().getString("banreason");
		this.is_muted = (config.getConfig().getBoolean("Is-muted"));
		this.mute_time = config.getConfig().getInt("Mute-time");
		this.on_team = config.getConfig().getBoolean("Team");
		this.team_name = config.getConfig().getString("TeamName");
		this.warns = config.getConfig().getInt("Warns");
 
	}

	public boolean is_Muted() {
		return is_muted;
	}
	public boolean on_Team(){
		return on_team;
	}
	
	public boolean is_Banned() {
		return is_banned;
	}
	public String ban_Reason(){
		return ban_reason;
	}
	public String Team_Name(){
		return team_name;
	}
	public int mute_Time() {
		return mute_time;
	}
	public int Warns(){
		return warns;
	}
}
