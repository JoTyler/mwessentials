package com.minewreck.mwessentials.playerInfo;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.minewreck.mwessentials.teams.TeamData;

public class FileCreation implements Listener {
	
	@EventHandler
	public void saveFile(PlayerQuitEvent e){
		if(PlayerData.data.containsKey(e.getPlayer())) {
			CustomConfig config = new CustomConfig(e.getPlayer().getUniqueId());
			config.getConfig().set("Name", e.getPlayer().getName());
			config.getConfig().set("UUID", e.getPlayer().getUniqueId().toString());
			config.getConfig().set("Is-banned", PlayerData.data.get(e.getPlayer()).is_banned);
			config.getConfig().set("banreason", PlayerData.data.get(e.getPlayer()).ban_reason);
			config.getConfig().set("Team", PlayerData.data.get(e.getPlayer()).on_team);
			config.getConfig().set("TeamName", PlayerData.data.get(e.getPlayer()).team_name);
			config.getConfig().set("Warns", PlayerData.data.get(e.getPlayer()).warns);
			CustomConfig.saveConfig(e.getPlayer().getUniqueId());
			PlayerData.data.remove(e.getPlayer());
			e.setQuitMessage("");
		}
	}
	
	@EventHandler
	public void createFile(PlayerJoinEvent e) {
		
		CustomConfig config = new CustomConfig(e.getPlayer().getUniqueId());
		
		if(!config.configExists(e.getPlayer().getUniqueId())) {
		
			config.getConfig().set("Name", e.getPlayer().getName());
			config.getConfig().set("UUID", e.getPlayer().getUniqueId().toString());
			config.getConfig().set("Is-banned", false);
			config.getConfig().set("banreason", "none");
			config.getConfig().set("Team", false);
			config.getConfig().set("TeamName", "");
			config.getConfig().set("Warns", 0);
			
			CustomConfig.saveConfig(e.getPlayer().getUniqueId());
			
		}
		if(config.getConfig().getBoolean("Team")){
			String TeamName = config.getConfig().getString("TeamName");
			TeamData.data.put(TeamName , new TeamData(TeamName));
		}
			
		PlayerData.data.put(e.getPlayer(), new PlayerData(e.getPlayer()));
		
		

	}
	

}
