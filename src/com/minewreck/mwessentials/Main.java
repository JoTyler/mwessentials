package com.minewreck.mwessentials;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;



import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.minewreck.mwessentials.control.DutyCommand;
import com.minewreck.mwessentials.playerInfo.CustomConfig;
import com.minewreck.mwessentials.playerInfo.FileCreation;
import com.minewreck.mwessentials.playerInfo.InfoCommand;
import com.minewreck.mwessentials.playerInfo.PlayerData;
import com.minewreck.mwessentials.punnishments.BanCommand;
import com.minewreck.mwessentials.punnishments.KickCommand;
import com.minewreck.mwessentials.punnishments.MuteCommand;
import com.minewreck.mwessentials.punnishments.UnMuteCommand;
import com.minewreck.mwessentials.punnishments.UnbanCommand;
import com.minewreck.mwessentials.punnishments.WarnCommand;
import com.minewreck.mwessentials.redeems.Redeemable;
import com.minewreck.mwessentials.redeems.RedeemCommand;
import com.minewreck.mwessentials.supplycrates.SpawnCrate;
import com.minewreck.mwessentials.teams.TeamCommand;
import com.minewreck.mwessentials.teams.TeamData;
import com.minewreck.mwessentials.teams.TeamPvp;
import com.minewreck.mwessentials.voting.Rewards;
import com.minewreck.mwessentials.voting.VoteListener;

public class Main extends JavaPlugin {

	static Main instance;
	public static FileConfiguration config;
	public static HashSet<Redeemable> kits = new HashSet<Redeemable>();
	public static HashMap<Player, Integer> timeremaning = new HashMap<Player, Integer>();
	public static ArrayList<Location> crateLoc = new ArrayList<Location>();
//	public static ProtocolManager protocalManager;

	@Override
	public void onEnable() {
		instance = this;
		//Main.protocalManager = ProtocolLibrary.getProtocolManager();	

		giveRedeem();
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new BanCommand(), this);
		pm.registerEvents(new MuteCommand(), this);
		pm.registerEvents(new FileCreation(), this);
		pm.registerEvents(new JoinMessage(), this);
		pm.registerEvents(new RedeemCommand(), this);
		pm.registerEvents(new TeamPvp(), this);
		pm.registerEvents(new SpawnCrate(),this);
		pm.registerEvents(new VoteListener(), this);
		pm.registerEvents(new RedeemCommand(), this);

		getCommand("ban").setExecutor(new BanCommand());
		getCommand("unban").setExecutor(new UnbanCommand());
		getCommand("kick").setExecutor(new KickCommand());
		getCommand("mute").setExecutor(new MuteCommand());
		getCommand("unmute").setExecutor(new UnMuteCommand());
		getCommand("redeem").setExecutor(new RedeemCommand());
		getCommand("team").setExecutor(new TeamCommand());
		getCommand("cp").setExecutor(new SpawnCrate());
		getCommand("vote").setExecutor(new Rewards());
		getCommand("warn").setExecutor(new WarnCommand());
		getCommand("pinfo").setExecutor(new InfoCommand());
		getCommand("duty").setExecutor(new DutyCommand());


		for (Player all : Bukkit.getOnlinePlayers()){
			PlayerData.data.put(all, new PlayerData(all));
		CustomConfig config = new CustomConfig(all.getUniqueId());
		if(config.getConfig().getBoolean("Team")){
			String TeamName = config.getConfig().getString("TeamName");
			TeamData.data.put(TeamName , new TeamData(TeamName));
		}
		}
	}
	

	
	public void Creeper(){
		ItemStack icon = new ItemStack(Material.STONE, 1);
		ItemMeta meta = icon.getItemMeta();
		meta.setDisplayName(ChatColor.GREEN + "Creeper");
		icon.setItemMeta(meta);
		List<ItemStack> rewards = new ArrayList<ItemStack>();
		//Blocks
		ItemStack IronB = new ItemStack(Material.IRON_BLOCK, 32);
		ItemStack EmeraldB = new ItemStack(Material.EMERALD_BLOCK, 32);
		ItemStack GoldB = new ItemStack(Material.GOLD_BLOCK, 32);
		ItemStack DiamondB = new ItemStack(Material.DIAMOND_BLOCK, 32);
		ItemStack TNT = new ItemStack(Material.TNT, 64);
		ItemStack TNT2 = new ItemStack(Material.TNT, 56);
		//Armour 
		ItemStack Helm = new ItemStack(Material.DIAMOND_HELMET, 1);
		ItemMeta EnchantMeta = Helm.getItemMeta();
		EnchantMeta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 4, true);
		Helm.setItemMeta(EnchantMeta);
		ItemStack Chest = new ItemStack(Material.DIAMOND_CHESTPLATE, 1);
		Chest.setItemMeta(EnchantMeta);
		ItemStack Leg = new ItemStack(Material.DIAMOND_LEGGINGS, 1);
		Leg.setItemMeta(EnchantMeta);
		ItemStack Boot = new ItemStack(Material.DIAMOND_BOOTS, 1);
		Boot.setItemMeta(EnchantMeta);
		//Weapons
		ItemStack Sword = new ItemStack(Material.DIAMOND_SWORD, 1);
		ItemMeta SwordMeta = Sword.getItemMeta();
		SwordMeta.addEnchant(Enchantment.DAMAGE_ALL, 4, true);
		SwordMeta.addEnchant(Enchantment.FIRE_ASPECT, 1, true);
		Sword.setItemMeta(SwordMeta);
		ItemStack Bow = new ItemStack(Material.BOW, 1);
		ItemMeta BowMeta = Bow.getItemMeta();
		BowMeta.addEnchant(Enchantment.ARROW_INFINITE, 1, true);
		BowMeta.addEnchant(Enchantment.ARROW_KNOCKBACK, 3, true);
		BowMeta.addEnchant(Enchantment.ARROW_FIRE, 1, true);
		Bow.setItemMeta(BowMeta);
		//ETC
		ItemStack Arrow = new ItemStack(Material.ARROW, 1);
		ItemStack DPic = new ItemStack(Material.DIAMOND_PICKAXE, 1);
		ItemMeta DPicMeta = DPic.getItemMeta();
		DPicMeta.addEnchant(Enchantment.DURABILITY, 1, true);
		DPicMeta.addEnchant(Enchantment.SILK_TOUCH, 1, true);
		DPic.setItemMeta(DPicMeta);
		//Add to List
		rewards.add(IronB);
		rewards.add(EmeraldB);
		rewards.add(GoldB);
		rewards.add(DiamondB);
		rewards.add(TNT);
		rewards.add(TNT2);
		rewards.add(Helm);
		rewards.add(Chest);
		rewards.add(Leg);
		rewards.add(Boot);
		rewards.add(Sword);
		rewards.add(Bow);
		rewards.add(Arrow);
		rewards.add(DPic);


		kits.add(new Redeemable("Creeper", icon, rewards, 25000, 250));
	}

	public void Blaze(){
		ItemStack icon = new ItemStack(Material.STONE, 1);
		ItemMeta meta = icon.getItemMeta();
		meta.setDisplayName(ChatColor.GREEN + "Blaze");
		icon.setItemMeta(meta);
		List<ItemStack> rewards = new ArrayList<ItemStack>();
		//Blocks
		ItemStack IronB = new ItemStack(Material.IRON_BLOCK, 64);
		ItemStack EmeraldB = new ItemStack(Material.EMERALD_BLOCK, 64);
		ItemStack GoldB = new ItemStack(Material.GOLD_BLOCK, 64);
		ItemStack DiamondB = new ItemStack(Material.DIAMOND_BLOCK, 64);
		ItemStack TNT = new ItemStack(Material.TNT, 64);
		ItemStack TNT2 = new ItemStack(Material.TNT, 64);
		ItemStack TNT3 = new ItemStack(Material.TNT, 52);
		//Armour 
		ItemStack Helm = new ItemStack(Material.DIAMOND_HELMET, 3);
		ItemMeta EnchantMeta = Helm.getItemMeta();
		EnchantMeta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 4, true);
		Helm.setItemMeta(EnchantMeta);
		ItemStack Chest = new ItemStack(Material.DIAMOND_CHESTPLATE, 3);
		Chest.setItemMeta(EnchantMeta);
		ItemStack Leg = new ItemStack(Material.DIAMOND_LEGGINGS, 3);
		Leg.setItemMeta(EnchantMeta);
		ItemStack Boot = new ItemStack(Material.DIAMOND_BOOTS, 3);
		Boot.setItemMeta(EnchantMeta);
		//Weapons
		ItemStack Sword = new ItemStack(Material.DIAMOND_SWORD, 3);
		ItemMeta SwordMeta = Sword.getItemMeta();
		SwordMeta.addEnchant(Enchantment.DAMAGE_ALL, 4, true);
		SwordMeta.addEnchant(Enchantment.FIRE_ASPECT, 1, true);
		Sword.setItemMeta(SwordMeta);
		ItemStack Bow = new ItemStack(Material.BOW, 3);
		ItemMeta BowMeta = Bow.getItemMeta();
		BowMeta.addEnchant(Enchantment.ARROW_INFINITE, 1, true);
		BowMeta.addEnchant(Enchantment.ARROW_KNOCKBACK, 3, true);
		BowMeta.addEnchant(Enchantment.ARROW_FIRE, 1, true);
		Bow.setItemMeta(BowMeta);
		//ETC
		ItemStack Arrow = new ItemStack(Material.ARROW, 1);
		ItemStack DPic = new ItemStack(Material.DIAMOND_PICKAXE, 3);
		ItemMeta DPicMeta = DPic.getItemMeta();
		DPicMeta.addEnchant(Enchantment.DURABILITY, 1, true);
		DPicMeta.addEnchant(Enchantment.SILK_TOUCH, 1, true);
		DPic.setItemMeta(DPicMeta);
		//Add to List
		rewards.add(IronB);
		rewards.add(EmeraldB);
		rewards.add(GoldB);
		rewards.add(DiamondB);
		rewards.add(TNT);
		rewards.add(TNT2);
		rewards.add(TNT3);
		rewards.add(Helm);
		rewards.add(Chest);
		rewards.add(Leg);
		rewards.add(Boot);
		rewards.add(Sword);
		rewards.add(Bow);
		rewards.add(Arrow);
		rewards.add(DPic);


		kits.add(new Redeemable("Blaze", icon, rewards, 87500, 500));
	}
	public void Ghast(){
		ItemStack icon = new ItemStack(Material.STONE, 1);
		ItemMeta meta = icon.getItemMeta();
		meta.setDisplayName(ChatColor.GREEN + "Ghast");
		icon.setItemMeta(meta);
		List<ItemStack> rewards = new ArrayList<ItemStack>();
		//Blocks
		ItemStack IronB = new ItemStack(Material.IRON_BLOCK, 64);
		ItemStack IronB2 = new ItemStack(Material.IRON_BLOCK, 32);
		ItemStack EmeraldB = new ItemStack(Material.EMERALD_BLOCK, 64);
		ItemStack EmeraldB2 = new ItemStack(Material.EMERALD_BLOCK, 32);
		ItemStack GoldB = new ItemStack(Material.GOLD_BLOCK, 64);
		ItemStack GoldB2 = new ItemStack(Material.GOLD_BLOCK, 32);
		ItemStack DiamondB = new ItemStack(Material.DIAMOND_BLOCK, 64);
		ItemStack DiamondB2 = new ItemStack(Material.DIAMOND_BLOCK, 32);
		ItemStack TNT = new ItemStack(Material.TNT, 64);
		ItemStack TNT2 = new ItemStack(Material.TNT, 64);
		ItemStack TNT3 = new ItemStack(Material.TNT, 64);
		ItemStack TNT4 = new ItemStack(Material.TNT, 48);
		//Armour 
		ItemStack Helm = new ItemStack(Material.DIAMOND_HELMET, 3);
		ItemMeta EnchantMeta = Helm.getItemMeta();
		EnchantMeta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 4, true);
		Helm.setItemMeta(EnchantMeta);
		ItemStack Chest = new ItemStack(Material.DIAMOND_CHESTPLATE, 3);
		Chest.setItemMeta(EnchantMeta);
		ItemStack Leg = new ItemStack(Material.DIAMOND_LEGGINGS, 3);
		Leg.setItemMeta(EnchantMeta);
		ItemStack Boot = new ItemStack(Material.DIAMOND_BOOTS, 3);
		Boot.setItemMeta(EnchantMeta);
		//Weapons
		ItemStack Sword = new ItemStack(Material.DIAMOND_SWORD, 3);
		ItemMeta SwordMeta = Sword.getItemMeta();
		SwordMeta.addEnchant(Enchantment.DAMAGE_ALL, 4, true);
		SwordMeta.addEnchant(Enchantment.FIRE_ASPECT, 1, true);
		Sword.setItemMeta(SwordMeta);
		ItemStack Bow = new ItemStack(Material.BOW, 3);
		ItemMeta BowMeta = Bow.getItemMeta();
		BowMeta.addEnchant(Enchantment.ARROW_INFINITE, 1, true);
		BowMeta.addEnchant(Enchantment.ARROW_KNOCKBACK, 3, true);
		BowMeta.addEnchant(Enchantment.ARROW_FIRE, 1, true);
		Bow.setItemMeta(BowMeta);
		//ETC
		ItemStack Arrow = new ItemStack(Material.ARROW, 1);
		ItemStack DPic = new ItemStack(Material.DIAMOND_PICKAXE, 3);
		ItemMeta DPicMeta = DPic.getItemMeta();
		DPicMeta.addEnchant(Enchantment.DURABILITY, 1, true);
		DPicMeta.addEnchant(Enchantment.SILK_TOUCH, 1, true);
		ItemStack EPearl = new ItemStack(Material.ENDER_PEARL, 64);
		ItemStack QBlock = new ItemStack(Material.QUARTZ_BLOCK, 64);
		ItemStack Obby = new ItemStack(Material.OBSIDIAN, 64);
		ItemStack Obby2 = new ItemStack(Material.OBSIDIAN, 64);
		ItemStack Obby3 = new ItemStack(Material.OBSIDIAN, 64);
		DPic.setItemMeta(DPicMeta);
		//Add to List
		rewards.add(IronB);
		rewards.add(EmeraldB);
		rewards.add(GoldB);
		rewards.add(DiamondB);
		rewards.add(IronB2);
		rewards.add(EmeraldB2);
		rewards.add(GoldB2);
		rewards.add(DiamondB2);
		rewards.add(TNT);
		rewards.add(TNT2);
		rewards.add(TNT3);
		rewards.add(TNT4);
		rewards.add(Helm);
		rewards.add(Chest);
		rewards.add(Leg);
		rewards.add(Boot);
		rewards.add(Sword);
		rewards.add(Bow);
		rewards.add(Arrow);
		rewards.add(DPic);
		rewards.add(EPearl);
		rewards.add(QBlock);
		rewards.add(Obby);
		rewards.add(Obby2);
		rewards.add(Obby3);

		kits.add(new Redeemable("Ghast", icon, rewards, 212500, 750));
	}
	public void Ender(){
		ItemStack icon = new ItemStack(Material.STONE, 1);
		ItemMeta meta = icon.getItemMeta();
		meta.setDisplayName(ChatColor.GREEN + "Ender");
		icon.setItemMeta(meta);
		List<ItemStack> rewards = new ArrayList<ItemStack>();
		//Blocks
		ItemStack IronB = new ItemStack(Material.IRON_BLOCK, 64);
		ItemStack IronB2 = new ItemStack(Material.IRON_BLOCK, 56);
		ItemStack EmeraldB = new ItemStack(Material.EMERALD_BLOCK, 64);
		ItemStack EmeraldB2 = new ItemStack(Material.EMERALD_BLOCK, 56);
		ItemStack GoldB = new ItemStack(Material.GOLD_BLOCK, 64);
		ItemStack GoldB2 = new ItemStack(Material.GOLD_BLOCK, 56);
		ItemStack DiamondB = new ItemStack(Material.DIAMOND_BLOCK, 64);
		ItemStack DiamondB2 = new ItemStack(Material.DIAMOND_BLOCK, 56);
		ItemStack TNT = new ItemStack(Material.TNT, 64);
		ItemStack TNT2 = new ItemStack(Material.TNT, 64);
		ItemStack TNT3 = new ItemStack(Material.TNT, 64);
		ItemStack TNT4 = new ItemStack(Material.TNT, 64);
		ItemStack TNT5 = new ItemStack(Material.TNT, 44);
		//Armour 
		ItemStack Helm = new ItemStack(Material.DIAMOND_HELMET, 5);
		ItemMeta EnchantMeta = Helm.getItemMeta();
		EnchantMeta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 4, true);
		Helm.setItemMeta(EnchantMeta);
		ItemStack Chest = new ItemStack(Material.DIAMOND_CHESTPLATE, 5);
		Chest.setItemMeta(EnchantMeta);
		ItemStack Leg = new ItemStack(Material.DIAMOND_LEGGINGS, 5);
		Leg.setItemMeta(EnchantMeta);
		ItemStack Boot = new ItemStack(Material.DIAMOND_BOOTS, 5);
		Boot.setItemMeta(EnchantMeta);
		//Weapons
		ItemStack Sword = new ItemStack(Material.DIAMOND_SWORD, 5);
		ItemMeta SwordMeta = Sword.getItemMeta();
		SwordMeta.addEnchant(Enchantment.DAMAGE_ALL, 4, true);
		SwordMeta.addEnchant(Enchantment.FIRE_ASPECT, 1, true);
		Sword.setItemMeta(SwordMeta);
		ItemStack Bow = new ItemStack(Material.BOW, 5);
		ItemMeta BowMeta = Bow.getItemMeta();
		BowMeta.addEnchant(Enchantment.ARROW_INFINITE, 1, true);
		BowMeta.addEnchant(Enchantment.ARROW_KNOCKBACK, 3, true);
		BowMeta.addEnchant(Enchantment.ARROW_FIRE, 1, true);
		Bow.setItemMeta(BowMeta);
		//ETC
		ItemStack Arrow = new ItemStack(Material.ARROW, 1);
		ItemStack DPic = new ItemStack(Material.DIAMOND_PICKAXE, 5);
		ItemMeta DPicMeta = DPic.getItemMeta();
		DPicMeta.addEnchant(Enchantment.DURABILITY, 1, true);
		DPicMeta.addEnchant(Enchantment.SILK_TOUCH, 1, true);
		ItemStack EPearl = new ItemStack(Material.ENDER_PEARL, 64);
		ItemStack EPearl2 = new ItemStack(Material.ENDER_PEARL, 64);
		ItemStack QBlock = new ItemStack(Material.QUARTZ_BLOCK, 64);
		ItemStack QBlock2 = new ItemStack(Material.QUARTZ_BLOCK, 64);
		ItemStack Obby = new ItemStack(Material.OBSIDIAN, 64);
		ItemStack Obby2 = new ItemStack(Material.OBSIDIAN, 64);
		ItemStack Obby3= new ItemStack(Material.OBSIDIAN, 64);
		DPic.setItemMeta(DPicMeta);
		//Add to List
		rewards.add(IronB);
		rewards.add(EmeraldB);
		rewards.add(GoldB);
		rewards.add(DiamondB);
		rewards.add(IronB2);
		rewards.add(EmeraldB2);
		rewards.add(GoldB2);
		rewards.add(DiamondB2);
		rewards.add(TNT);
		rewards.add(TNT2);
		rewards.add(TNT3);
		rewards.add(TNT4);
		rewards.add(TNT5);
		rewards.add(Helm);
		rewards.add(Chest);
		rewards.add(Leg);
		rewards.add(Boot);
		rewards.add(Sword);
		rewards.add(Bow);
		rewards.add(Arrow);
		rewards.add(DPic);
		rewards.add(EPearl);
		rewards.add(QBlock);
		rewards.add(EPearl2);
		rewards.add(QBlock2);
		rewards.add(Obby);
		rewards.add(Obby2);
		rewards.add(Obby3);

		kits.add(new Redeemable("Ender", icon, rewards, 337500, 7500));
	}
	public void Hero(){
		ItemStack icon = new ItemStack(Material.STONE, 1);
		ItemMeta meta = icon.getItemMeta();
		meta.setDisplayName(ChatColor.GREEN + "Hero");
		icon.setItemMeta(meta);
		List<ItemStack> rewards = new ArrayList<ItemStack>();
		//Blocks
		ItemStack IronB = new ItemStack(Material.IRON_BLOCK, 64);
		ItemStack IronB2 = new ItemStack(Material.IRON_BLOCK, 64);
		ItemStack EmeraldB = new ItemStack(Material.EMERALD_BLOCK, 64);
		ItemStack EmeraldB2 = new ItemStack(Material.EMERALD_BLOCK, 64);
		ItemStack GoldB = new ItemStack(Material.GOLD_BLOCK, 64);
		ItemStack GoldB2 = new ItemStack(Material.GOLD_BLOCK, 64);
		ItemStack DiamondB = new ItemStack(Material.DIAMOND_BLOCK, 64);
		ItemStack DiamondB2 = new ItemStack(Material.DIAMOND_BLOCK, 64);
		ItemStack TNT = new ItemStack(Material.TNT, 64);
		ItemStack TNT2 = new ItemStack(Material.TNT, 64);
		ItemStack TNT3 = new ItemStack(Material.TNT, 64);
		ItemStack TNT4 = new ItemStack(Material.TNT, 64);
		ItemStack TNT5 = new ItemStack(Material.TNT, 64);
		ItemStack IronB3 = new ItemStack(Material.IRON_BLOCK, 16);
		ItemStack EmeraldB3 = new ItemStack(Material.EMERALD_BLOCK, 16);
		ItemStack GoldB3 = new ItemStack(Material.GOLD_BLOCK, 16);
		ItemStack DiamondB3 = new ItemStack(Material.DIAMOND_BLOCK, 16);
		//Armour 
		ItemStack Helm = new ItemStack(Material.DIAMOND_HELMET, 8);
		ItemMeta EnchantMeta = Helm.getItemMeta();
		EnchantMeta.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 4, true);
		Helm.setItemMeta(EnchantMeta);
		ItemStack Chest = new ItemStack(Material.DIAMOND_CHESTPLATE, 8);
		Chest.setItemMeta(EnchantMeta);
		ItemStack Leg = new ItemStack(Material.DIAMOND_LEGGINGS, 8);
		Leg.setItemMeta(EnchantMeta);
		ItemStack Boot = new ItemStack(Material.DIAMOND_BOOTS, 8);
		Boot.setItemMeta(EnchantMeta);
		//Weapons
		ItemStack Sword = new ItemStack(Material.DIAMOND_SWORD, 8);
		ItemMeta SwordMeta = Sword.getItemMeta();
		SwordMeta.addEnchant(Enchantment.DAMAGE_ALL, 4, true);
		SwordMeta.addEnchant(Enchantment.FIRE_ASPECT, 1, true);
		Sword.setItemMeta(SwordMeta);
		ItemStack Bow = new ItemStack(Material.BOW, 8);
		ItemMeta BowMeta = Bow.getItemMeta();
		BowMeta.addEnchant(Enchantment.ARROW_INFINITE, 1, true);
		BowMeta.addEnchant(Enchantment.ARROW_KNOCKBACK, 3, true);
		BowMeta.addEnchant(Enchantment.ARROW_FIRE, 1, true);
		Bow.setItemMeta(BowMeta);
		//ETC
		ItemStack Arrow = new ItemStack(Material.ARROW, 1);
		ItemStack DPic = new ItemStack(Material.DIAMOND_PICKAXE, 8);
		ItemMeta DPicMeta = DPic.getItemMeta();
		DPicMeta.addEnchant(Enchantment.DURABILITY, 1, true);
		DPicMeta.addEnchant(Enchantment.SILK_TOUCH, 1, true);
		ItemStack EPearl = new ItemStack(Material.ENDER_PEARL, 64);
		ItemStack EPearl2 = new ItemStack(Material.ENDER_PEARL, 64);
		ItemStack QBlock = new ItemStack(Material.QUARTZ_BLOCK, 64);
		ItemStack QBlock2 = new ItemStack(Material.QUARTZ_BLOCK, 64);
		ItemStack Obby = new ItemStack(Material.OBSIDIAN, 64);
		ItemStack Obby2 = new ItemStack(Material.OBSIDIAN, 64);
		ItemStack Obby3 = new ItemStack(Material.OBSIDIAN, 64);
		ItemStack Obby4 = new ItemStack(Material.OBSIDIAN, 64);
		ItemStack Obby5 = new ItemStack(Material.OBSIDIAN, 64);
		DPic.setItemMeta(DPicMeta);
		//Add to List
		rewards.add(IronB);
		rewards.add(EmeraldB);
		rewards.add(GoldB);
		rewards.add(DiamondB);
		rewards.add(TNT);
		rewards.add(IronB2);
		rewards.add(EmeraldB2);
		rewards.add(GoldB2);
		rewards.add(DiamondB2);
		rewards.add(TNT2);
		rewards.add(IronB3);
		rewards.add(EmeraldB3);
		rewards.add(GoldB3);
		rewards.add(DiamondB3);
		rewards.add(TNT3);
		rewards.add(TNT4);
		rewards.add(TNT5);
		rewards.add(Helm);
		rewards.add(Chest);
		rewards.add(Leg);
		rewards.add(Boot);
		rewards.add(Sword);
		rewards.add(Bow);
		rewards.add(Arrow);
		rewards.add(DPic);
		rewards.add(EPearl);
		rewards.add(EPearl2);
		rewards.add(QBlock);
		rewards.add(QBlock2);
		rewards.add(Obby);
		rewards.add(Obby2);
		rewards.add(Obby3);
		rewards.add(Obby4);
		rewards.add(Obby5);

		kits.add(new Redeemable("Hero", icon, rewards, 649500, 12000));
	}

	
	public void giveRedeem() {
	  Creeper();
	  Blaze();
	  Ghast();
	  Ender();
	  Hero();
	}

	@Override
	public void onDisable() {
		saveConfig();

		for(Location loc : crateLoc){
			Block air = loc.getBlock();
			air.setType(Material.AIR);
			
		}
		for (Player all : Bukkit.getOnlinePlayers()) {
			if (PlayerData.data.containsKey(all)) {
				CustomConfig config = new CustomConfig(all.getUniqueId());
				config.getConfig().set("Name", all.getName());
				config.getConfig().set("UUID", all.getUniqueId().toString());
				config.getConfig().set("Is-banned",
						PlayerData.data.get(all).is_Banned());
				config.getConfig().set("banreason",
						PlayerData.data.get(all).ban_Reason());
				config.getConfig().set("Team",
						PlayerData.data.get(all).on_Team());
				config.getConfig().set("TeamName",
						PlayerData.data.get(all).Team_Name());
				config.getConfig().set("Warns", PlayerData.data.get(all).Warns());
				CustomConfig.saveConfig(all.getUniqueId());
				PlayerData.data.remove(all);
			}
			
		}
	}

	public static Main getInstance() {
		return instance;
	}

}