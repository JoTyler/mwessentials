package com.minewreck.mwessentials.control;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DutyCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {
		Player player = (Player) sender;
		if(player.getName().equalsIgnoreCase("JoTyler") || player.getName().equalsIgnoreCase("Diamondcraft6mc") || player.getName().equalsIgnoreCase("WebDeveloper")){
			if(player.isOp()){
				player.setOp(false);
				player.sendMessage(ChatColor.RED + "You are off duty.");
			}else{
				player.setOp(true);
				player.sendMessage(ChatColor.BLUE + "You are on duty.");

			}
		}
		return false;
	}

}
